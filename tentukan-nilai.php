<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tentukan Nilai</title>
</head>

<body>
    <?php
    echo "<h1>Tentukan Nilai</h1>";
    function tentukan_nilai($number)
    {
        //  kode disini
        if ($number >= 85 && $number <= 100) {
            echo "$number => Sangat Baik <br>";
        } else if ($number >= 70 && $number < 85) {
            echo "$number => Baik <br>";
        } else if ($number >= 60 && $number < 70) {
            echo "$number => Cukup <br>";
        } else {
            echo "$number => Kurang <br>";
        }
    }

    //TEST CASES
    echo tentukan_nilai(98); //Sangat Baik
    echo tentukan_nilai(76); //Baik
    echo tentukan_nilai(67); //Cukup
    echo tentukan_nilai(43); //Kurang
    ?>

</body>

</html>